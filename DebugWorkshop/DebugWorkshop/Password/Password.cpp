#include <iostream>

struct Password
{
	char value[16];
	bool incorrect;
	Password() : value(""), incorrect(true)
	{
	}
};

int main()
{
	std::cout << "Enter your password to continue:" << std::endl;
	Password pwd;
	std::cin >> pwd.value;

	if (!strcmp(pwd.value, "********"))
		pwd.incorrect = false;

	if(!pwd.incorrect)
		std::cout << "Congratulations\n";

	return 0;
}

// the problam with this function is that everyone who enters password with size(16) gets the output: "Congratulations" even if he got it wrong
//the bug happens because of (strcmp) methoud, in order to fix it we can use the library of string and just compare like this:
// if (pwd.value == "*s*******")...